######################################################################
# Automatically generated by qmake (3.0) Sa. Apr. 1 00:00:06 2017
######################################################################

QT += gui core widgets
TEMPLATE = app
TARGET = schabi_cortex
INCLUDEPATH += .
LIBS += -lm

# Input
HEADERS += src/cubeReality.hpp src/network.hpp src/point.hpp src/cubeReality.hpp src/ui/plotWidget.hpp \
    src/runner.h \
    src/random.h
SOURCES += src/main.cpp src/cubeReality.cpp src/ui/plotWidget.cpp \
    src/network.cpp \
    src/runner.cpp
