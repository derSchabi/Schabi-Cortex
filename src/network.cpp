#include "network.hpp"
#include "random.h"
#include "point.hpp"


//#define use_inflated

namespace KI
{

Network::Network() {  
    matrix = new Neuron[NW * NH];
#ifdef use_inflated
    for(int y = 0; y < NW; y++) {
        for(int x = 0; x < NW; x++) {
            matrix[x+(y*NW)].x = (x*2.5);
            matrix[x+(y*NW)].y = (y*2.5);
        }
    }
#else
    for(int i = 0; i < NW*NH; i++) {
        matrix[i].x = getRand()/10 + 45;
        matrix[i].y = getRand()/10 + 45;
    }
#endif
}

long Network::getNearestNeuron(Point e)
{
    double lowestDistance = 1000000000000;
    long nearestNeuron = -1;
    for(int i = 0; i < NW*NH; i++) {
        double d = calcDistance(matrix[i], e);
        if(d < lowestDistance) {
            lowestDistance = d;
            nearestNeuron = i;
        }
    }
    return nearestNeuron;
}

void Network::adjustNeuron(long nId, Point e, double factor)
{
    Vector dV = calcDistanceVector(matrix[nId], e);
    matrix[nId].x += dV.x * factor;
    matrix[nId].y += dV.y * factor;
}

void Network::adjustNet(long nId, Point e, double factor)
{
    adjustNeuron(nId, e, factor);
    // and neighbourhood
    long left = nId + 1;
    long right = nId -1;
    long up = nId - NW;
    long down = nId + NW;

    if(left < NW * NH && left/NW == nId/NW) {
        adjustNeuron(left, e, factor/2);
    }
    if(right >= 0 && right/NW == nId/NW) {
        adjustNeuron(right, e, factor/2);
    }
    if(up >= 0) {
        adjustNeuron(up, e, factor/2);
    }
    if(down < NW * NH) {
        adjustNeuron(down, e, factor/2);
    }
}

}
