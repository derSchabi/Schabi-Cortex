#pragma once
#include "point.hpp"

#include <math.h>


namespace KI
{

const static int NW = 10;
const static int NH = 10;

class Network
{
public:
    Network();

    Neuron* getNetworkData() {
        return matrix;
    }

    long getNearestNeuron(Point e);
    void adjustNeuron(long nId, Point e, double factor);
    void adjustNet(long nId, Point e, double factor);

private:
    Neuron *matrix;

    Vector calcDistanceVector(Neuron n, Point e) {
        return Vector(e.x - n.x, e.y - n.y);
    }

    double calcDistance(Neuron n, Point e) {
        Vector dV = calcDistanceVector(n, e);
        return sqrt(dV.x*dV.x + dV.y*dV.y);
    }
};

}
