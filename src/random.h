#ifndef RANDOM_H
#define RANDOM_H

#include <stdlib.h>

namespace KI
{

double getRand() {
    return (double)(((long)rand() << 32) | rand()) / 100000000000000000.0;
}

}

#endif // RANDOM_H

