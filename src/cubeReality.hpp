#pragma once

#include "point.hpp"

namespace KI
{

class CubeReality
{
public:
    Point getEvent();
};

}
