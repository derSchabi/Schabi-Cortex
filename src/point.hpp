#pragma once

namespace KI
{

struct Point
{
public:
    double x;
    double y;

    Point() {
        x = y = 0;
    }

    Point(double x, double y) {
        this->x = x;
        this->y = y;
    }

    Point getScaledBy(double s) {
        return Point(s*x, s*y);
    }
};

typedef Point Vector;
typedef Point Neuron;

}
