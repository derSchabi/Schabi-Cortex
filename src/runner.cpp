#include "runner.h"

Runner::Runner(void (*func)(), QObject *parent) : QObject(parent)
{
    this->func = func;
}


void Runner::nextIteration()
{
    func();
}
