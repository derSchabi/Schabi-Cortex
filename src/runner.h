#ifndef RUNNER_H
#define RUNNER_H

#include <QObject>

#include "point.hpp"
#include "network.hpp"

class Runner : public QObject
{
    Q_OBJECT
public:
    explicit Runner(void (*func)(), QObject *parent = 0);

signals:

public slots:
    void nextIteration();

private:
    void (*func)();
};

#endif // RUNNER_H
