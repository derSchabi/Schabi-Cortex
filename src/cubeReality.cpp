#include "cubeReality.hpp"
#include "point.hpp"

#include <math.h>
#include <stdlib.h>

namespace KI
{

const double lowRangeX = 0;
const double highRangeX = 100;
const double rangeX = highRangeX - lowRangeX;

const double lowRangeY = 0;
const double highRangeY = 100;
const double rangeY = highRangeY - lowRangeY;

double getRand();
double fittBoundery(const double, const double);

Point CubeReality::getEvent() {
    // simulate an event by using random
    double dx;
    double dy;
    do{
        dx = lowRangeX + fittBoundery(rangeX, getRand());
        dy = lowRangeY + fittBoundery(rangeY, getRand());
    } while(false);
    return Point(dx, dy);
}

double fittBoundery(const double range, const double input)
{
    double num = input;
    while(input > range)
    {
        num = num / range;
    }
    return num;
}

}
