#include "network.hpp"
#include "cubeReality.hpp"
#include "ui/plotWidget.hpp"
#include "runner.h"

#include <QApplication>
#include <QPushButton>
#include <stdlib.h>
#include <time.h>
#include <iostream>

QTimer *iTimer;
PlotWidget *plotWidget;
KI::CubeReality reality;
KI::Network network;

long iteration = 0;

const int IBR = 10000; //iterations before redraw

double factor = 0.6;

void processIteration() {
    KI::Point event;
    long nId;

    for(int i = 0; i < IBR; i++)
    {
        iteration++;
        event = reality.getEvent();
        nId = network.getNearestNeuron(event);
        // do magic
        network.adjustNet(nId, event, factor);
        factor *= 0.99998;
    }

    std::cout<<"Iteration: "<<iteration<<std::endl;
    plotWidget->setIterationCount(iteration);
    plotWidget->setLastDataPoint(event);
    plotWidget->setLastNetowkrData(network.getNetworkData(), KI::NW, KI::NH);
    plotWidget->setNearestNeuron(network.getNetworkData()[nId]);
    plotWidget->repaint();

    if(iteration > 500000) {
        iTimer->stop();
    }
}

int main(int argc, char **argv)
{
    QApplication app(argc, argv);
    Runner runner(processIteration);
    iTimer = new QTimer(&app);
    iTimer->setInterval(16);

    plotWidget = new PlotWidget();

    srand(time(NULL));

    QObject::connect(iTimer, SIGNAL(timeout()),
            &runner, SLOT(nextIteration()));

    plotWidget->show();

    iTimer->start();
    app.exec();
}
