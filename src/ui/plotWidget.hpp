#pragma once

#include <QWidget>
#include <QTimer>

#include "../point.hpp"
#include "../network.hpp"

class PlotWidget
    : public QWidget
{
    Q_OBJECT
public:
    PlotWidget(QWidget *parent = 0);
    
    void setLastDataPoint(KI::Point data) {
        lastDataPoint = data;
    }

    void setLastNetowkrData(KI::Neuron *data, int w, int h) {
        lastNetworkData = data;
        width = w;
        height = h;
    }

    void setNearestNeuron(KI::Neuron nn) {
        nearestNeuron = nn;
    }

    void setIterationCount(long count) {
        iterationCount = count;
    }

signals:
    void nextIteration();
   
private:
    virtual void paintEvent(QPaintEvent *);
    
    KI::Point lastDataPoint;
    KI::Neuron *lastNetworkData;
    KI::Neuron nearestNeuron;

    int width; int height;

    QTimer repaintTimer;
    long iterationCount;
};


