#include "plotWidget.hpp"
#include "../point.hpp"

#include <QPainter>
#include <QTimer>

//#define drawNE

double s(double);

PlotWidget::PlotWidget(QWidget *parent)
    :QWidget(parent)
    ,lastDataPoint(KI::Point(10, 10))
{
    setFixedSize(1000, 1000);
}

void PlotWidget::paintEvent(QPaintEvent *)
{
    QPainter painter(this);

    //darw background
    painter.drawRect(0, 0, size().width(), size().height());


    //draw network
    painter.setPen(QPen(Qt::black, 1));
    for(int y = 0; y < height; y++) {
        for(int x = 0; x < width; x++) {
            if(y < height-1) {
                if(x < width-1) {
                    KI::Neuron t; //this
                    KI::Neuron n; //next
                    KI::Neuron b; //beneathe
                    t = lastNetworkData[x + y*width];
                    n = lastNetworkData[x + y*width + 1];
                    b = lastNetworkData[x + (y+1)*width];
                    painter.drawLine(s(t.x), s(t.y), s(n.x), s(n.y));
                    painter.drawLine(s(t.x), s(t.y), s(b.x), s(b.y));
                } else {
                    KI::Neuron t; //this
                    KI::Neuron b; //beneathe
                    t = lastNetworkData[x + y*width];
                    b = lastNetworkData[x + (y+1)*width];
                    painter.drawLine(s(t.x), s(t.y), s(b.x), s(b.y));
                }
            } else {
                if(x < width - 1) {
                    KI::Neuron t; //this
                    KI::Neuron n; //next
                    t = lastNetworkData[x + y*width];
                    n = lastNetworkData[x + y*width + 1];
                    painter.drawLine(s(t.x), s(t.y), s(n.x), s(n.y));
                }
            }
        }
    }

#ifdef drawNE
    //draw data point
    painter.setPen(QPen(Qt::blue, 10));
    painter.drawPoint(s(lastDataPoint.x), s(lastDataPoint.y));

    // draw nearest neuron
    painter.setPen(QPen(Qt::red, 10));
    painter.drawPoint(s(nearestNeuron.x), s(nearestNeuron.y));
#endif

    painter.drawText(20, 20, QString::number(iterationCount));
}

double s(double raw) //scale
{
    return raw * 10.0;
}
